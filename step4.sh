#!/bin/bash

awk -F'=' '{ print $2 }' labels.txt > labels1.txt
LC_APP_PATH=$(sed -n 1'p' labels1.txt)
KIOSK_FILE= $(sed -n 2'p' labels1.txt)
LC_KIOSK_APP=$(sed -n 3'p' labels1.txt)
UC_KIOSK_APP=$(sed -n 4'p' labels1.txt)
APP_RLS=$(sed -n 5'p' labels1.txt)
CHG=$(sed -n 6'p' labels1.txt)

mkdir two/public
mkdir two/public/QA
PROJ_ROOT= public/$LC_APP_PATH
SWAWINSTAGING=one/new

cd $PROJ_ROOT
cp $KIOSK_FILE/$APP_RLS $SWAWINSTAGING