#!/bin/bash

awk -F'=' '{ print $2 }' labels.txt > labels1.txt
LC_APP_PATH=$(sed -n 1'p' labels1.txt)
KIOSK_FILE= $(sed -n 2'p' labels1.txt)
LC_KIOSK_APP=$(sed -n 3'p' labels1.txt)
UC_KIOSK_APP=$(sed -n 4'p' labels1.txt)
APP_RLS=$(sed -n 5'p' labels1.txt)
CHG=$(sed -n 6'p' labels1.txt)
SWASTAGING= /one/staging/kiosk_client
LC_APP_NAME=  ${LC_KIOSK_APP}_r${APP_RLS}
UC_APP_NAME=  ${UC_KIOSK_APP}_R${APP_RLS}
STAGE-ONLY= /one/staging/kiosk_client/
mkdir /one/staging/kiosk_client/DEV/

mkdir -p $SWASTAGING/$LC_KIOSK_APP/$LC_APP_NAME/$UC_APP_NAME_$CHG
cd $STAGE-ONLY
cp -Rfp * $SWASTAGING/$LC_KIOSK_APP/$LC_APP_NAME/$UC_APP_NAME_$CHG